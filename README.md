# README #

Istanbul loader for webpack to instrument files for code coverage.  Can be used with serverless offline to get code coverage results for integration tests.

## Usage
To use the loader add it to your webpack.config.js file.  You will only want to load for development / test build and not production.

### Example with Typescript
```
const loaders = [
    {
        loader: '@ephesoft/webpack.istanbul.loader' // Must be first loader
    },
    {
        loader: 'ts-loader',
        options: {
            transpileOnly: true
        }
    }
]

module.exports = {
    ...
    module: {
        rules: [
            {
                // Add instrumentation for test coverage on development builds
                use: getMode() === 'development' ? loaders : loaders.slice(1, 2)
            }
        ]
    }
}
```

## Setup
Ensure that project has the prerequisites:
```bash
npm ci
```

## NPM run targets
Run NPM commands as
```bash
npm run command
```
### Available Commands
|Command|Description|
|:-|:-|
| audit | Executes a dependency audit using the default NPM registry |
| audit:fix | Attempts an automatic fix of any audit failures. |
| build | Cleans the dist folder and transpiles/webpacks the code |
| clean | Deletes the working folders like dist and docs |
| clean:docs | Deletes the docs folder |
| docs | Create API documentation for the package in a "docs" folder |
| docs:bitbucket | Creates API documentation in Bitbucket markdown format in a "docs" folder |
| lint | Performs static analysis on the TypeScript files. |
| lint:fix | Performs static analysis on the TypeScript files and attempts to automatically fix errors. |
| lint:report | Performs static analysis on the TypeScript files using the default validation rules and logs results to a JSON file. |
| pack | Creates a local tarball package |
| prebuild | Automatically ran during build. |
| postbuild| Automatically ran during build. |
| postprepare  | Automatically ran during package install to setup tooling. |
| start | Instructs the TypeScript compiler to compile the ts files every time a change is detected. |
| test | Runs all tests. |
| test:unit | Runs the unit tests. |
| validate:ga | Validates the package version is valid for GA release. |
| validate:beta | Validates the package version is valid for a Beta release. |