/*  This was originally maintained by the webpack team.
    Webpack has retired and archived the original project.
    https://github.com/webpack-contrib/istanbul-instrumenter-loader
*/

import convert from 'convert-source-map';
import loaderUtils from 'loader-utils';
import schema from './options.json';
import validateOptions from 'schema-utils';
import { createInstrumenter } from 'istanbul-lib-instrument';

export default function (source: string, sourceMap: any) {
    const options = Object.assign({ produceSourceMap: true }, loaderUtils.getOptions(this));

    validateOptions(schema as any, options, {
        name: 'Istanbul Instrumenter Loader',
        baseDataPath: 'options'
    });

    let srcMap = sourceMap;
    // use inline source map, if any
    if (!srcMap) {
        const inlineSourceMap = convert.fromSource(source);
        if (inlineSourceMap) {
            srcMap = inlineSourceMap.sourcemap;
        }
    }

    const instrumenter = createInstrumenter(options);

    instrumenter.instrument(
        source,
        this.resourcePath,
        (error, instrumentedSource) => {
            this.callback(error, instrumentedSource, instrumenter.lastSourceMap());
        },
        srcMap
    );
}
