# Contributing
When contributing to this repository, all changes must be on a new branch which goes through a pull request process.  If there is an open Jira item for this work the branch name should start with the Jira item #.

Review the [README](README.md) on how the project is setup.

## Making Changes
1. Create a named feature branch starting with the Jira item #, like `SEM-1_Fixing_Something`.
2. Make the code changes
3. Write tests for your change and test the code changes - see below
4. Update the version using [Semantic Versioning](https://semver.org/): package.json
5. Update CHANGELOG.md
6. Submit a Pull Request.

## Testing
1. Install npm packages: `npm install`
2. Check for vulnerabilities: `npm audit`
3. Run unit tests: `npm run test`
4. Run linting: `npm run lint`
5. Build using: `npm run build`

- - - - -
Following [Semantic Versioning](https://semver.org/)