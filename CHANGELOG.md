# Changelog

## 2.2.0 (2022-10-21)
- Removed internal package references

## 2.1.0 (2022-10-05)
- Updated npm references
- Updated to template version 6.0.2

## 2.0.0 (2022-09-26)
- BREAKING: Updated name to istanbul
- Fixed husky
- Updated build to node 16
- Updated packages to fix dependency issues
- Updated publish to NPM
- Updated README.md

## 1.0.0 (2021-12-14)
- Updated packages to fix audit

## 0.1.0 (2021-05-06)
- Updated for initial release

- - - - -
* Following [Semantic Versioning](https://semver.org/)
* [Changelog Formatting](https://ephesoft.atlassian.net/wiki/spaces/ZEUS1/pages/1189347481/Changelog+Formatting)
* [Major Version Migration Notes](MIGRATION.md)
